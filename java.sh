#!/usr/bin/env bash

sudo yum install git -y
wget https://download.java.net/java/GA/jdk14/076bab302c7b4508975440c56f6cc26a/36/GPL/openjdk-14_linux-x64_bin.tar.gz
tar xzf openjdk-14_linux-x64_bin.tar.gz
mv jdk-14 /opt
cd /opt/jdk-14
export JAVA_HOME=/opt/jdk-14/
export JRE_HOME=/opt/jdk-14/jre/
export PATH=$PATH:/opt/jdk-14/bin:/opt/jdk-14/jre/bin


function clone_pull {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}

clone_pull https://gitlab.com/efe136/demo1.git
#git clone https://gitlab.com/efe136/demo1.git && cd "$(basename "$_" .git)"

chmod +x mvnw

./mvnw clean package
