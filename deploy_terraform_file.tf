        provider "aws" {
                profile = "default"
                region  = "eu-central-1"
                        }

        resource "aws_vpc" "naimv_vpc" {
        cidr_block = "172.16.0.0/16"
        tags = {
                Name = "naimv-vpc"
                }
                                        }
        resource "aws_subnet" "naimv_subnet" {
        vpc_id            = aws_vpc.naimv_vpc.id
        cidr_block        = "172.16.10.0/24"
        availability_zone = "eu-central-1a"
        tags = {
        Name = "naim-subnet"
                }
                                                }
        resource "aws_network_interface" "ens33" {
        subnet_id   = aws_subnet.naimv_subnet.id
        private_ips = ["172.16.10.100"]

        tags = {
                Name = "primary_network_interface"
                }
                                                }


#pet-app ec2

        resource "aws_instance" "pet-app" {
        #name = "pet-app"
        ami           = "ami-000cbb96a79217336"
        instance_type = "t3.micro"
        vpc_security_group_ids = [aws_security_group.naimv_sec.id]
        tags = {
                name = "pet-app"
                }

                                        }



#MYSQL_DB_ec2
        resource "aws_instance" "mysql-db" {

        #Name = "mysql-db"
        ami           = "ami-000cbb96a79217336"
        instance_type = "t3.micro"

        vpc_security_group_ids = [aws_security_group.naimv_sec.id]

        tags =  {
                Name = "mysql_db"

                }

      }


        resource "aws_security_group" "naimv_sec" {
        name        = "naimv_sec"
        description = "Allow TLS inbound traffic"


        ingress {
        description = "Allow SSH"
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["172.16.0.0/16"]
                }


        egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
                }
        tags = {
        Name = "naimv_sec"
                }
                                                }
